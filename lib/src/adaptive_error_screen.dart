import 'dart:io' show SocketException;

import 'package:flutter/material.dart';
import 'package:error_screen/error_screen.dart';

class AdaptiveErrorScreen extends StatelessWidget {

  final Object? error;
  final VoidCallback onRetry;
  final Widget? Function(Object? error)? customBuilder;

  const AdaptiveErrorScreen({ super.key, required this.error, required this.onRetry, this.customBuilder });

  @override
  Widget build(BuildContext context) {
    if (error is SocketException)
      return NetworkErrorScreen(onRetry: onRetry);

    if (customBuilder != null) {
      var widget = customBuilder!(error);
      if (widget != null)
        return widget;
    }

    return WrongThingScreen(onTap: onRetry);
  }
}
