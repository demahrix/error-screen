import 'package:flutter/material.dart';
import 'package:error_screen/src/images.dart';
import 'package:error_screen/src/error_screen_base.dart';

class NetworkErrorScreen extends ErrorScreenBase {

  NetworkErrorScreen({
    super.direction  = Axis.vertical,
    required VoidCallback onRetry
  }): super(
    image: Image.asset(ErrorScreenImages.NO_NETWORK, width: 256.0, package: "error_screen"),
    title: Text("Erreur de connection"),
    description: Text("Veuillez vérifier votre connection internet de réessayer plus tard"),
    button: ElevatedButton(
      onPressed: onRetry,
      style: ButtonStyle(
        padding: MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 30.0, vertical: 13.0)),
        elevation: MaterialStateProperty.all<double>(0.0),
        minimumSize: MaterialStateProperty.all(Size(120.0, 48.0))
      ),
      child: Text("Réesayez")
    ),
  );

}
