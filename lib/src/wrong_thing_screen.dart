// va utiliser error screen
import 'package:error_screen/src/images.dart';
import 'package:flutter/material.dart';
import 'package:error_screen/src/error_screen_base.dart';

class WrongThingScreen extends ErrorScreenBase {

  WrongThingScreen({
    super.direction = Axis.vertical,
    super.title = const Text('Quelque chose c\'est mal passé'),
    super.description = const Text('Une erreur inatendu est survénue !'),
    Widget? image,
    Widget buttonChild = const Text('Réessayer'),
    VoidCallback? onTap
  }): super(
    image: image ?? Image.asset(ErrorScreenImages.ERROR, width: 192.0, package: 'error_screen'),
    button: onTap == null ? null : ElevatedButton(
      onPressed: onTap,
      style: ButtonStyle(
        padding: MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 30.0, vertical: 13.0)),
        elevation: MaterialStateProperty.all<double>(0.0),
        minimumSize: MaterialStateProperty.all(Size(120.0, 48.0))
      ),
      child: buttonChild
    ),
  );

}
