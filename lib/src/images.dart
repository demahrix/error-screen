
class ErrorScreenImages {

  static const EMPTY_STATES = 'assets/empty_states.webp';

  static const NO_NETWORK = 'assets/no_network.webp';
  static const NO_NETWORK_2 = 'assets/no_network_2.webp';

  static const ERROR = 'assets/error.webp';
  static const ERROR_2 = 'assets/error.png';

  static const $404 = 'assets/404.png';

}

