import "package:flutter/material.dart";

class ErrorScreenBase extends StatelessWidget {

  // Dans un alignment de type horizontal les elements seront centres de maniere horizontal et vertical

  static TextStyle _titleStyle(TextTheme theme) => TextStyle(
    fontSize: 23.0,
    fontWeight: FontWeight.w600,
    color: theme.bodyLarge!.color
  );

  static TextStyle _descriptionStyle(TextTheme theme) => TextStyle(
    color: theme.bodySmall!.color
  );

  final Axis direction;
  final Widget image;
  final Widget? title;
  final Widget? description;
  final Widget? button;

  ErrorScreenBase({
    this.direction = Axis.vertical,
    required this.image,
    this.title,
    this.description,
    this.button
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;

    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center, // vertical center,
        children: [

          if (direction == Axis.horizontal)
            image,

          if (direction == Axis.horizontal)
            const SizedBox(width: 40.0),

          Column(
            mainAxisAlignment: MainAxisAlignment.center, // center l'image et le texte en mode horizonatal
            //mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              if (direction == Axis.vertical)
                image,

              if (direction == Axis.vertical)
                const SizedBox(height: 30.0),

              if (title != null)
                DefaultTextStyle(
                  style: _titleStyle(theme),
                  child: title!
                ),

              if (title != null)
                const SizedBox(height: 10.0),

              if (description != null)
                DefaultTextStyle(
                  style: _descriptionStyle(theme),
                  child: description!
                ),

              if (description != null)
                const SizedBox(height: 10.0),

              if (button != null)
                button!

            ],
          )

        ],
      ),
    );
  }

  /*Widget _handleTitle(Widget title) {
    DefaultTextStyle(style: null,

    )

    if (!(title is Text))
      return title;

    if (title.style == null)
      title.style = _titleStyle;

    title.style?.merge(TextStyle(
      fontWeight: FontWeight.bold
    ));

   n title;
  }*/

}
