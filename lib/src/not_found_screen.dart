import 'package:flutter/material.dart';
import '../error_screen.dart';

class NotFoundScreen extends ErrorScreenBase {

  NotFoundScreen({
    super.direction = Axis.vertical,
    super.title = const Text("Non trouvé"),
    super.description = const Text("Ce element n'existe pas"),
    String buttonLabel = "Aller à l'acceuil",
    VoidCallback? onTap
  }): super(
    image: Image.asset(ErrorScreenImages.EMPTY_STATES, width: 192.0, package: "error_screen"),
    button: onTap == null ? null : Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: ElevatedButton(
        onPressed: onTap,
        style: ButtonStyle(
          padding: MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 30.0, vertical: 13.0)),
          elevation: MaterialStateProperty.all<double>(0.0),
          minimumSize: MaterialStateProperty.all(Size(120.0, 48.0))
        ),
        child: Text(buttonLabel)
      ),
    ),
  );

}
